---
title: Persistent Symmetric Keys in OpenPGP
docname: draft-ietf-openpgp-persistent-symmetric-keys-01
category: std
ipr: trust200902
updates: 9580
area: sec
keyword: Internet-Draft
stand_alone: no
submissionType: IETF
pi:
  toc: yes
  tocdepth: 4
  sortrefs: yes
  symrefs: yes
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/twisstle/openpgp-persistent-symmetric-keys"
  latest: "https://twisstle.gitlab.io/openpgp-persistent-symmetric-keys/"
author:
  -
    ins: D. Huigens
    name: Daniel Huigens
    role: editor
    org: Proton AG
    street: Route de la Galaise 32
    code: 1228
    city: Plan-les-Ouates
    country: Switzerland
    email: d.huigens@protonmail.com
normative:
  RFC2104:
  RFC2119:
  RFC9580:
informative:
  Grover:
    title: Quantum mechanics helps in searching for a needle in a haystack
    author:
      -
        ins: L. Grover
    date: 1997
    target: https://arxiv.org/abs/quant-ph/9706033
  Shor:
    title: "Polynomial-Time Algorithms for Prime Factorization and Discrete Logarithms on a Quantum Computer"
    date: October 1997
    author:
      -
        ins: P. Shor
    target: http://dx.doi.org/10.1137/S0097539795293172
  PQCinOpenPGP:
    title: Post-Quantum Cryptography in OpenPGP
    date: 19 October 2023
    author:
      -
        ins: S. Kousidis
      -
        ins: F. Strenzke
      -
        ins: A. Wussler
    target: https://datatracker.ietf.org/doc/html/draft-wussler-openpgp-pqc-03
--- abstract

This document defines new algorithms for the OpenPGP standard (RFC 9580) to support persistent symmetric keys, for message encryption using authenticated encryption with additional data (AEAD) and for authentication with hash-based message authentication codes (HMAC).
This enables the use of symmetric cryptography for data storage (and other contexts that do not require asymmetric cryptography), for improved performance, smaller keys, and improved resistance to quantum computing.

--- middle

# Introduction

The OpenPGP standard [RFC9580] has supported symmetric encryption for data packets using session keys since its inception, as well as symmetric encryption using password-derived keys.
This document extends the use of symmetric cryptography by adding support for persistent symmetric keys which can be stored in a transferable private key, and used to symmetrically encrypt session keys, for long-term storage and archival of messages.
This document uses authenticated encryption with associated data (AEAD) as defined by [RFC9580].

The OpenPGP standard also supports the use of digital signatures for authentication and integrity but no similar symmetric mechanism exists in the standard.
This document introduces hash-based message authentication codes (HMAC) as a symmetric counterpart to digital signatures, for long-term storage and archival of attestations of authenticity and certification.

# Conventions Used in This Document

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [RFC2119].
Any implementation that adheres to the format and methods specified in this document is called a compliant application.
Compliant applications are a subset of the broader set of OpenPGP applications described in [RFC9580].
Any [RFC2119] keyword within this document applies to compliant applications only.

# Motivation

When compared to asymmetric cryptography, symmetric cryptography can provide improved performance and equivalent security with smaller keys.
In contexts that do not require asymmetric cryptography, such as secure data storage where the same user encrypts and decrypts data, symmetric cryptography can be used to take advantage of these benefits.

Additionally, asymmetric algorithms included in OpenPGP are vulnerable to attacks that might become possible on quantum computers [Shor].
Symmetric cryptography is also affected by quantum computing but to a lesser extent, which can be countered by using larger keys [Grover].
While the standardization of quantum-secure asymmetric cryptography in OpenPGP is ongoing [PQCinOpenPGP], and will be required to secure communications, there is a large body of existing messages encrypted with classical algorithms.
Once persistent symmetric keys are available, these messages can be protected against future compromises efficiently by symmetrically re-encrypting the session key, and storing the message symmetrically encrypted for long-term storage and archival.

# Reusing and Renaming Packets

Rather than introducing new packets for storing persistent symmetric keys, the existing Secret-Key packets are reused for this purpose.
To indicate the type of keys, two algorithms (AEAD and HMAC) are registered, whose IDs can be used in the place of public-key algorithm IDs.
To accommodate these additions, we propose renaming the Public Key Algorithms registry to Persistent Key Algorithms.

Similarly, we reuse the Signature packet for "symmetric signatures".
For session keys encrypted with persistent symmetric keys, while a Symmetric-Key Encrypted Session Key packet exists, its semantics don't match our requirements, as it's intended to encrypt the session key with a user-provided password, and doesn't offer a way to store a reference to a persistent key.
Therefore, we reuse the Public-Key Encrypted Session Key packet instead, which does offer the desired semantics.
Nevertheless, given this usage, the naming of these packets may be confusing, so we propose to rename them to "String-to-Key Encrypted Session Key packet" and "Persistent Key Encrypted Session Key packet", instead.

# Persistent Symmetric Key Algorithms {#key-algos}

This document defines two new algorithms for use with OpenPGP, extending table 18 of [RFC9580].

In addition, it reserves space for future, private and experimental persistent symmetric key algorithms.

{: title="Persistent Symmetric Key Algorithm registrations" #key-algo-registrations}
ID | Algorithm | Public Key Format | Secret Key Format | Signature Format | PKESK Format
---:|--------------------------|---|---|---|---
 128 | AEAD | sym. algo, AEAD algo, fingerprint seed \[{{key-aead}}] | key material | N/A | IV, ciphertext \[{{pkesk-aead}}]
 129 | HMAC {{RFC2104}} | hash algo, fingerprint seed \[{{key-hmac}}] | key material | authentication tag \[{{sig-hmac}}] | N/A
 130 to 140 | Reserved for Future Persistent Symmetric Key Algorithms
 200 to 210 | Private or Experimental Persistent Symmetric Key Algorithms

These algorithm IDs can be used to store symmetric key material in Secret-Key Packets and Secret-Subkey packets (see section 5.5.3 of [RFC9580]).
The AEAD algorithm ID (and future, private or experimental symmetric encryption algorithms) can be used to store session keys encrypted using AEAD in PKESK packets (see section 5.1 of [RFC9580]).
The HMAC algorithm ID (and future, private or experimental symmetric authentication algorithms) can be used to store HMAC-based signatures in Signature packets (see section 5.2 of [RFC9580]).

Symmetric algorithm IDs MUST only be used with v6 keys, as defined in [RFC9580], or newer versions defined by updates of that document.

As the secret key material is required for all cryptographic operations with symmetric keys, implementations SHOULD NOT use symmetric algorithm IDs in Public-Key Packets or Public-Subkey Packets, and SHOULD NOT export Public-Key Packets from Secret-Key Packets holding symmetric key material.

When storing encrypted symmetric key material in a Secret-Key Packet or Secret-Subkey Packet, AEAD encryption (S2K usage octet 253, see section 3.7.2.1 of [RFC9580]) MUST be used, to ensure that the secret key material is bound to the fingerprint.
Implementations MUST NOT decrypt symmetric key material in a Secret-Key Packet or Secret-Subkey Packet that was encrypted using a different method.

## Algorithm-Specific Fields for AEAD keys {#key-aead}

The public key is this series of values:

- A one-octet symmetric algorithm identifier (see section 9.3 of [RFC9580])

- A one-octet AEAD algorithm (see section 9.6 of [RFC9580])

- A 32-octet random seed to randomize the key fingerprint

The secret key is this single value:

- Symmetric key material of the appropriate length for the given symmetric algorithm

## Algorithm-Specific Fields for HMAC keys {#key-hmac}

The public key is this series of values:

- A one-octet hash algorithm identifier (see section 9.5 of [RFC9580])

- A 32-octet random seed to randomize the key fingerprint

The secret key is this single value:

- Symmetric key material of the length of the hash output size of the given hash algorithm

## Algorithm-Specific Fields for AEAD encryption {#pkesk-aead}

- A starting initialization vector of size specified by AEAD mode

- A symmetric key encryption of the plaintext value described in section 5.1 of [RFC9580], performed using the selected symmetric-key cipher operating in the given AEAD mode, including the authentication tag.

## Algorithm-Specific Fields for HMAC signatures {#sig-hmac}

- An authentication tag of appropriate length for the hash algorithm

Although not required by HMAC, to maintain consistency with existing signature algorithms, HMAC tags are produced from appropriately hashed data, as per section 5.2.4 of [RFC9580].

# Security Considerations

Security considerations are discussed throughout the document where appropriate.

# IANA Considerations

## Updates to Public Key Algorithms

IANA is requested to rename the "OpenPGP Public Key Algorithms" registry to "OpenPGP Persistent Key Algorithms", and add the entries in {{key-algo-registrations}} to the registry.

## Updates to Packet Type Descriptions

IANA is requested to modify the "OpenPGP Packet Types" registry as follows:

- For Packet Tag 1 ("Public-Key Encrypted Session Key Packet"), change the Packet Type Description to "Persistent Key Encrypted Session Key Packet".
- For Packet Tag 3 ("Symmetric-Key Encrypted Session Key Packet"), change the Packet Type Description to "String-to-Key Encrypted Session Key Packet".

# Acknowledgements

An initial version of this draft was written by Dan Ristea (Proton AG), with guidance from Dr Philipp Jovanovic (University College London) and the editor.

--- back

# Test Vectors

To help implementing this specification a set of non-normative examples follow here.

## Transferable Secret Key {#sample-sk}

Here is a Transferable Secret Key consisting of:

- A v6 HMAC Private-Key packet
- A v6 direct key self-signature
- A User ID packet
- A v6 positive certification self-signature
- A v6 AEAD Private-Subkey packet
- A v6 subkey binding signature

The primary key has the fingerprint `39d3d9b684974edecfa0a31ccfc7a646eca61ee616a42d8e18e2741110994ac7`.

The subkey has the fingerprint `8431344883f607c31fc112c501b26fb4c3a3696b0f1a1accf3eba78107dc2b3a`.

{: sourcecode-name="v6-sample-sk.asc"}
~~~ application/pgp-keys
{::include test-vectors/v6-sample-sk.asc}
~~~

## Encrypted and Signed Message

Here is the message "Testing\n" encrypted and signed using the secret key {{sample-sk}}, consisting of:

- A v6 PKESK packet
- A v2 SEIPD packet

The hex-encoded AEAD key used to encrypt the session key is `144d188b5618c745156d1c9ea7c42714e97cb0705189c01e1c4c4b05a65a08ee`.

The hex-encoded session key is `2ff2190e39a3b12fdf35b0da30a2895f215628cdb237b58686da8d017da59acb`.

{: sourcecode-name="v6-sample-message.asc"}
~~~ application/octet-stream
{::include test-vectors/v6-sample-message.asc}
~~~
